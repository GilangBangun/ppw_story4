from django.test import TestCase

# Create your tests here.

class story8(TestCase):
    def test_url_exist(self):
        response = self.client.get('/story8')
        self.assertEqual(response.status_code, 200)

    def test_template(self):
        response = self.client.get('/story8')
        self.assertTemplateUsed(response, 'story8/story8.html')

    def test_story8_data_is_exist(self):
        response = self.client.get('/story8/data/?q=test')
        content = str(response.content, encoding='utf8')
        self.assertIn('{"kind"', content)
