from django.urls import path
from . import views

#app_name = 'Matkul'

urlpatterns = [
    path('matkul', views.matkul_view, name='matkul_view'),
    path('matkul_create', views.matkul_create, name='matkul_create'),
    path('matkul_update/<str:pk>/', views.matkul_update, name='matkul_update'),
    path('matkul_delete/<str:pk>/', views.matkul_delete, name='matkul_delete'),
    path('<slug:slug>/', views.matkul_detail, name="detail"),
    

]