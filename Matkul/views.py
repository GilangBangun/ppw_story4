from django.shortcuts import render, redirect
from .models import Matkul
from .forms import MatkulForm

# Create your views here.
def matkul_view(request):
    matkuls = Matkul.objects.all().order_by('nama')
    return render(request, 'Matkul/matkul_view.html', {'matkuls':matkuls})

def matkul_create(request):
    form = MatkulForm()
    if request.method == 'POST':
        form = MatkulForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/matkul')

    return render(request, 'Matkul/matkul_form.html', {'form':form})

def matkul_update(request, pk):

    matkul = Matkul.objects.get(id=pk)
    form = MatkulForm(instance=matkul)

    if request.method == 'POST':
        form = MatkulForm(request.POST, instance=matkul)
        if form.is_valid():
            form.save()
            return redirect('/matkul')

    return render(request, 'Matkul/matkul_form.html', {'form':form})

def matkul_delete(request,pk):
    matkul = Matkul.objects.get(id=pk)
    matkul.delete()
    return redirect('/matkul')


def matkul_detail(request,slug):
    matkul = Matkul.objects.get(slug=slug)
    return render(request, 'Matkul/matkul_detail.html', {'matkul':matkul})
