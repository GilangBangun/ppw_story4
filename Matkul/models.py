import string
import random
from django.db import models
from django.utils.text import slugify

def rand_slug():
    return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(6))

# Create your models here.
class Matkul(models.Model):
    nama = models.CharField(max_length=100)
    dosen = models.CharField(max_length=100)
    sks = models.IntegerField()
    deskripsi = models.TextField()
    semester = models.CharField(max_length=100)
    ruang = models.IntegerField()

    #slug = models.SlugField()
    slug = models.SlugField(max_length=100, unique=True)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(rand_slug() + "-" + self.nama)
        super(Matkul, self).save(*args, **kwargs)

    def __str__(self):
        return self.nama
