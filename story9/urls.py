from django.urls import path
from . import views

app_name = 'story9'

urlpatterns = [

    path('story9/home', views.story9_home, name='story9_home'),
    path('story9/signup', views.signup, name='story9_signup'),

    path('story9/login', views.login_view, name='story9_login'),
    path('story9/logout', views.logout_view, name='story9_logout'),

 
]