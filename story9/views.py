from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.shortcuts import render, redirect

from .forms import SignUpForm


# Create your views here.
def story9_home(request):
    return render(request,'story9/home.html')

def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            # username = form.cleaned_data.get("username")
            # raw_password = form.cleaned_data.get('password1')
            # user = authenticate(username = username, password = raw_password)
            # login(request, user)
            return redirect('/story9/login')
    else:
        form = SignUpForm()
    return render(request, 'story9/signup.html', {'form': form})

def login_view(request):
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username = username, password = password)

        if user is not None:
            login(request, user)
            if request.user.is_authenticated:
                username = request.user.username
            
            if request.user.first_name is not None:
                first_name = ", " + request.user.first_name
                return render(request,'story9/home.html', {'username': username, 'first_name':first_name})
            
            return render(request,'story9/home.html', {'username': username})

    else:
        form = AuthenticationForm()
    return render(request, 'story9/login.html', {'form': form})

def logout_view(request):
    logout(request)
    text = "Anda berhasil Log Out"
    return render(request, 'story9/home.html', {"text": text})