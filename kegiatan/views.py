from django.shortcuts import render, redirect
from .models import Kegiatan
from .models import Peserta
from .forms import KegiatanForm
from .forms import PesertaForm

# Create your views here.
def kegiatan_view(request):
    kegiatans = Kegiatan.objects.all().order_by('nama')
    pesertas = Peserta.objects.all().order_by('nama')
    return render(request, 'kegiatan/kegiatan_view.html', {'kegiatans':kegiatans, 'pesertas':pesertas})

def kegiatan_create(request):
    form = KegiatanForm()
    if request.method == 'POST':
        form = KegiatanForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/kegiatan')

    return render(request, 'kegiatan/kegiatan_form.html', {'form':form})

def peserta_create(request, id):
    form = PesertaForm()
    if request.method == 'POST':
        kegiatans = Kegiatan.objects.get(id=id)
        nama_peserta = request.POST.get('nama')
        Peserta.objects.create(kegiatan = kegiatans, nama = nama_peserta)
        return redirect('/kegiatan')

    return render(request, 'kegiatan/peserta_form.html', {'form':form})
