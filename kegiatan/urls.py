from django.urls import path
from . import views

#app_name = 'Kegiatan'

urlpatterns = [
    path('kegiatan', views.kegiatan_view, name='kegiatan_view'),
    path('kegiatan_create', views.kegiatan_create, name='kegiatan_create'),
    path('peserta_create/<int:id>', views.peserta_create, name='peserta_create'),
    
]