from django.shortcuts import render


def home(request):
    return render(request, 'main/home.html')

def index0(request):
    return render(request, 'main/index0.html')

def index(request):
    return render(request, 'main/index.html')

def index2(request):
    return render(request, 'main/index2.html')

def index3(request):
    return render(request, 'main/index3.html')

