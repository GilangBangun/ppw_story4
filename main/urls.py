from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('home', views.home, name='home'),
    path('', views.index0, name='index0'),
    path('index', views.index, name='index'),
    path('index2', views.index2, name='index2'),
    path('index3', views.index3, name='index3'),
]
